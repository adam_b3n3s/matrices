import copy
class Complex:
    def __init__(self, Real, Imaginary):
        if type(Real) != float and type(Real) != int:
            raise Exception(str(Real) + " Is not a number. Only numbers can be put in Complex")
        if type(Imaginary) != int and type(Imaginary) != float:
            raise Exception(str(Imaginary) + " Is not a number. Only numbers can be put in Complex")
        self.real = Real
        self.imaginary = Imaginary
    def abs(a):
        """Vrátí absolutní hodnotu daného komplexního čísla"""
        return Complex((a.real*a.real+a.imaginary*a.imaginary)**(1/2), 0)
    def sum(a, b):
        """Vrátí součet dvou komplexních čísel"""
        return Complex(a.real + b.real, a.imaginary + b.imaginary)
    def difference(a, b):
        """Vrátí rozdíl dvou komplexních čísel"""
        return Complex(a.real - b.real, a.imaginary - b.imaginary)
    def multiply(a, b):
        """Vrátí vynásobení dvou komplexních čísel"""
        return Complex(a.real*b.real-a.imaginary*b.imaginary, a.real*b.imaginary + b.real*a.imaginary)
    def division(a,b):
        """Vrátí podíl dvou komplexních čísel, pokud druhé číslo je nulové nelze dělit nulou a vrátí None"""
        if b.real == 0 and b.imaginary == 0:
            return None
        return Complex(((a.real * b.real)+(a.imaginary*b.imaginary))/(b.real**2 + b.imaginary**2), 
        (a.imaginary*b.real - a.real*b.imaginary)/(b.real**2 + b.imaginary**2))
    def conjugate(a):
        """Vrátí sdružené komplexní číslo"""
        return Complex(a.real, -a.imaginary)
    def print(a):
        print(Complex.returnString(a))
    def returnString(a):
        """Vrátí "hezky" upravený string v hodnotě daného komplexního čísla"""
        if a.imaginary == 0:
            return(a.real)
        elif a.real == 0:
            return(str(a.imaginary)+"i")
        elif a.imaginary == 0 and a.real == 0:
            return(0)
        else:
            if a.imaginary < 0:
                if a.imaginary == -1:
                    return(str(a.real)+"-"+"i")
                else:
                    return(str(a.real)+str(a.imaginary)+"i")
            else: 
                if a.imaginary == 1:
                    return(str(a.real)+"+"+"i")
                else:
                    return(str(a.real)+"+"+str(a.imaginary)+"i")
class Matrix:
    def __init__(self, a = [[]]):
        i = 0
        while i < len(a):
            if len(a[i]) != len(a[0]):
                raise Exception("Only matrix, can be put in Matrix")
            i = i + 1
        i = 0
        j = 0
        while i < len(a):
            while j < len(a[0]):
                if type(a[i][j]) != Complex and type(a[i][j]) != int and type(a[i][j]) != float:
                    raise Exception(str(a[i][j]) + " Is not a number. Only numbers or complex numbers, can be put in Matrix")
                j = j + 1
            i = i + 1
        self.m = len(a)
        if a == [[]]:
            self.m = 0
        self.n = len(a[0])
        i = 0
        j = 0
        while i < self.m:
            j = 0
            while j < self.n:
                if type(a[i][j]) != Complex:
                    a[i][j] = Complex(a[i][j], 0)
                j = j + 1
            i = i + 1
        self.matrix = a
    def print(a):
        """Vytiskne matici do konzole"""
        print(Matrix.returnString(a))
    def returnString(a):
        """Vrátí matici jako string"""
        i = 0
        j = 0
        helper = " "
        lengh = 0
        while i < a.m:
            j = 0
            while j < a.n:
                if len(str(Complex.returnString(a.matrix[i][j]))) > lengh:
                    lengh = len(str(Complex.returnString(a.matrix[i][j])))
                j = j + 1
            i = i + 1
        helper = " " * lengh
        i = 0
        j = 0
        thisWillBeReturned = ""
        while i < a.m:
            s = ""
            j = 0
            while j < a.n:
                helperCopy = helper
                helperCopy = helperCopy[:len(helper)-len(str(Complex.returnString(a.matrix[i][j])))+1]
                s = s + str(Complex.returnString(a.matrix[i][j])) + helperCopy
                j = j + 1
            while s[-1] == " ":
                s = s[:-1]
            if thisWillBeReturned == "":
                thisWillBeReturned = s
            else:
                thisWillBeReturned = thisWillBeReturned + "\n" + s
            i = i + 1
        return thisWillBeReturned
    def square(self):
        """Vrátí True pokud je matice čtvercová a False pokud matice není čtvercová"""
        if self.m == self.n:
            return True
        return False
    def transpose(self):
        """Vrátí transponovanou matici"""
        i = 0
        j = 0
        b = []
        c = []
        while i < self.n:
            j = 0
            while j < self.m:
                b.append(self.matrix[j][i])
                j = j + 1
            c.append(b)
            b = []
            i = i + 1
        d = Matrix(c)
        i, j = 0, 0
        return d
    def sum(a, b):
        """Vrátí součet dvou matic stejného typu pokud matic nejsou stejného typu vyhodí výjimku"""
        if a.m != b.m or a.n != b.n:
            raise Exception("Only same type matrix, can be added together")
        c = []
        d = []
        i = 0
        j = 0
        while i < a.m:
            j = 0
            while j < a.n:
                c.append(Complex.sum(a.matrix[i][j], b.matrix[i][j]))
                j = j + 1
            d.append(c)
            c = []
            newMatrix = Matrix(d)
            i = i + 1
        return newMatrix
    def hadamard(a, b):
        """Vrátí hadamardův součin dvou matic stejného typu pokud matic nejsou stejného typu vyhodí výjimku"""
        if a.m != b.m or a.n != b.n:
            raise Exception("Only same type matrix, can be hadamard together")
        c = []
        d = []
        i = 0
        j = 0
        while i < a.m:
            j = 0
            while j < a.n:
                c.append(Complex.multiply(a.matrix[i][j], b.matrix[i][j]))
                j = j + 1
            d.append(c)
            c = []
            newMatrix = Matrix(d)
            i = i + 1
        return newMatrix
    def kronecker(a, b):
        """Vrátí kroneckerův součin dvou matic"""
        i = 0
        d = []
        while i < a.m:
            k = 0
            while k < b.m:
                c = []
                j = 0
                while j < a.n:
                    c = c + b.lineSkalar(a.matrix[i][j], k).matrix[k]
                    j = j + 1
                d.append(c)
                k = k + 1
            i = i + 1
        return Matrix(d)
    def skalar(skalar, a):
        """Vrátí matici, kde vynásobý každý prvek matice skalárem"""
        if type(skalar) != Complex:
            skalar = Complex(skalar, 0)
        c = []
        d = []
        i = 0
        j = 0
        while i < a.m:
            j = 0
            while j < a.n:
                c.append(Complex.multiply(skalar, a.matrix[i][j]))
                j = j + 1
            d.append(c)
            c = []
            i = i + 1
        newMatrix = Matrix(d)
        return newMatrix
    def multiply(a, b):
        """Vynásobý dvě matice mezi sebou "klasickým" násobením matic, pokud jsou matice ve špatném tvaru vyhodí chybu"""
        if a.n != b.m:
            raise Exception("Not valid matrix \"a\" and \"b\"")
        d = []
        e = []
        intermediateCalculation = Complex(0, 0)
        i = 0
        j = 0
        k = 0
        while i < a.m:
            j = 0
            while j < b.n:
                while k < a.n:
                    intermediateCalculation = Complex.sum(intermediateCalculation, Complex.multiply(a.matrix[i][k], b.matrix[k][j]))
                    k = k + 1
                k = 0
                d.append(intermediateCalculation)
                intermediateCalculation = Complex(0, 0)
                j = j + 1
            e.append(d)
            d = []
            i = i + 1
        newMatrix = Matrix(e)
        return newMatrix
    def determinant(self):
        """Spočítá determinant dané matice, pokud není čtvercová vyhodí chybu"""
        if self.square() == False:
            raise Exception("Not valid matrix to compute determinant from")
        if self.m == 2:
            return Complex.difference(Complex.multiply(self.matrix[0][0],self.matrix[1][1]), Complex.multiply(self.matrix[0][1], self.matrix[1][0]))
        k = 0
        det = Complex(0, 0)
        b = []
        c = []
        while k < self.n:
            i = 1
            while i < self.m:
                j = 0
                while j < self.n:
                    if j != k:
                        b.append(self.matrix[i][j])
                    j = j + 1
                c.append(b)
                b = []
                i = i + 1
            newMatrix = Matrix(c)
            c = []
            det = Complex.sum(det, Complex.multiply(Complex.multiply(self.matrix[0][k], Complex(pow(-1, 2 + k), 0)), newMatrix.determinant()))
            k = k + 1
        return det
    def inverse(self):
        """Vrátí inverzní matici k dané matici, pokud není čtvercová vyhodí chybu"""
        if self.square() == False:
            raise Exception("Not valid matrix to compute reciprocal from")
        if self.m == 2:
            det = self.determinant()
            s = [[self.matrix[1][1], Complex.multiply(Complex(-1, 0), self.matrix[0][1])], 
            [Complex.multiply(Complex(-1, 0), self.matrix[1][0]), self.matrix[0][0]]]
            return Matrix.skalar(Complex.division(Complex(1, 0), det), Matrix(s))
        return Matrix.skalar(Complex.division(Complex(1, 0), self.determinant()), self.reciprocal())
    def round(self, epsilon = 0.0001):
        """Vrátí danou matici a zahodí desetiný rozvoj u prvků, které mají delší desetiný rozvoj než je epsilon
        epsilon je nepovinný parametr"""
        a = self
        i = 0
        j = 0
        while i < a.m:
            j = 0
            while j < a.n:
                a.matrix[i][j].real = int(a.matrix[i][j].real/(epsilon))
                a.matrix[i][j].real = a.matrix[i][j].real*epsilon
                a.matrix[i][j].imaginary = round(a.matrix[i][j].imaginary*(1/epsilon))
                a.matrix[i][j].imaginary = a.matrix[i][j].imaginary*epsilon
                if not ("e" in str(a.matrix[i][j].real)):
                    if len(str(a.matrix[i][j].real).split(".")[1])>len(str(epsilon))-2:
                        a.matrix[i][j].real = str(a.matrix[i][j].real)[:-1]
                        a.matrix[i][j].real = float(a.matrix[i][j].real)
                if not ("e" in str(a.matrix[i][j].imaginary)):
                    if len(str(a.matrix[i][j].imaginary).split(".")[1])>len(str(epsilon))-2:
                        a.matrix[i][j].imaginary = str(a.matrix[i][j].imaginary)[:-1]
                        a.matrix[i][j].imaginary = float(a.matrix[i][j].imaginary)
                j = j + 1
            i = i + 1
        return a
    def roundNull(self, epsilon = 0.00001):
        """Vrátí danou matici, kde zaokrouhlí čísla, která jsou téměř celá"""
        a = self
        i = 0
        j = 0
        while i < a.m:
            j = 0
            while j < a.n:
                if abs(a.matrix[i][j].real - round(a.matrix[i][j].real)) <= epsilon:
                    a.matrix[i][j].real = round(a.matrix[i][j].real)
                if abs(a.matrix[i][j].imaginary - round(a.matrix[i][j].imaginary)) <= epsilon:
                    a.matrix[i][j].imaginary = round(a.matrix[i][j].imaginary)
                j = j + 1
            i = i + 1
        return a
    def reciprocal(self):
        """Vrátí adjukovanou matici k dané matici, pokud není čtvercová vyhodí chybu"""
        if self.square() == False:
            raise Exception("Not valid matrix to compute reciprocal from")
        if self.m == 2:
            s = [[self.matrix[1][1], Complex.multiply(Complex(-1, 0), self.matrix[0][1])], 
            [Complex.multiply(Complex(-1, 0), self.matrix[1][0]), self.matrix[0][0]]]
            return Matrix(s)
        i = 0
        j = 0
        c = []
        b = []
        det = self.determinant()
        if det == 0:
            raise Exception("Not valid matrix to compute reciprocal from")
        while i < self.m:
            j = 0
            while j < self.n:
                d = []
                e = []
                p = 0
                while p < self.m:
                    q = 0
                    if p != j:
                        while q < self.n:
                            if q != i:
                                e.append(self.matrix[p][q])
                            q = q + 1
                        d.append(e)
                    e = []
                    p = p + 1
                newMatrix = Matrix(d)
                det = newMatrix.determinant()
                d = []
                b.append(Complex.multiply(Complex(pow(-1, i + j + 2), 0), det))
                j = j + 1
            c.append(b)
            b = []
            i = i + 1
        return Matrix(c)
    def isIdentity(self):
        """Vrátí True pokud je daná matice jednotková v opačném případě vrátí False, pokud není čtvercová vyhodí chybu"""
        if self.square() == False:
            raise Exception("Not valid matrix to decide from")
        i = 0
        j = 0
        while i < self.m:
            j = 0
            while j < self.n:
                if i == j:
                    if self.matrix[i][j].real != 1:
                        return False
                    if self.matrix[i][j].imaginary != 1:
                        return False
                else:
                    if self.matrix[i][j].real != 0:
                        return False
                    if self.matrix[i][j].imaginary != 0:
                        return False
                j = j + 1
            i = i + 1
        return True
    def isSkalar(self):
        """Vrátí True pokud je daná matice skalární v opačném případě vrátí False, pokud není čtvercová vyhodí chybu"""
        if self.square() == False:
            raise Exception("Not valid matrix to decide from")
        i = 0
        j = 0
        a = self.matrix[0][0].real
        b = self.matrix[0][0].imaginary
        while i < self.m:
            j = 0
            while j < self.n:
                if i == j:
                    if self.matrix[i][j].real != a:
                        return False
                    if self.matrix[i][j].imaginary != b:
                        return False
                else:
                    if self.matrix[i][j] != 0:
                        return False
                j = j + 1
            i = i + 1
        return True
    def isDiagonal(self):
        """Vrátí True pokud je daná matice diagonální v opačném případě vrátí False, pokud není čtvercová vyhodí chybu"""
        if self.square() == False:
            raise Exception("Not valid matrix to decide from")
        i = 0
        j = 0
        while i < self.m:
            j = 0
            while j < self.n:
                if i == j:
                    if self.matrix[i][j].real == 0 and self.matrix[i][j].imaginary == 0:
                        return False
                else:
                    if self.matrix[i][j].real != 0:
                        return False
                    if self.matrix[i][j].imaginary != 0:
                        return False
                j = j + 1
            i = i + 1
        return True
    def isSymetrical(self):
        """Vrátí True pokud je daná matice symetrická v opačném případě vrátí False, pokud není čtvercová vyhodí chybu"""
        if self.square() == False:
            raise Exception("Not valid matrix to decide from")
        i = 0
        j = 0
        while i < self.m:
            j = 0
            while j < self.n:
                if self.matrix[i][j].real != self.matrix[j][i].real:
                    return False
                if self.matrix[i][j].imaginary != self.matrix[j][i].imaginary:
                    return False
                j = j + 1
            i = i + 1
        return True
    def isAntiSymetrical(self):
        """Vrátí True pokud je daná matice antisymetrická v opačném případě vrátí False, pokud není čtvercová vyhodí chybu"""
        if self.square() == False:
            raise Exception("Not valid matrix to decide from")
        a = Matrix.skalar(Complex(-1, 0), self)
        b = self.transpose()
        i = 0
        j = 0
        while i < self.m:
            j = 0
            while j < self.n:
                if a.matrix[i][j].real != b.matrix[i][j].real:
                    return False
                if a.matrix[i][j].imaginary != b.matrix[i][j].imaginary:
                    return False
                j = j + 1
            i = i + 1
        return True
    def isNull(self):
        """Vrátí True pokud je daná matice nulová v opačném případě vrátí False"""
        i = 0
        j = 0
        while i < self.m:
            j = 0
            while j < self.n:
                if self.matrix[i][j].real != 0:
                    return False
                if self.matrix[i][j].imaginary != 0:
                    return False
                j = j + 1
            i = i + 1
        return True
    def isRegular(self):
        """Vrátí True pokud je daná matice regulární v opačném případě vrátí False"""
        if self.determinant().real != 0:
            return True
        if self.determinant().imaginary != 0:
            return True
        return False
    def isSingular(self):
        """Vrátí True pokud je daná matice singulární v opačném případě vrátí False, pokud není čtvercová vyhodí chybu"""
        if self.determinant().real == 0 and self.determinant().imaginary == 0:
            return True
        return False
    def isHermit(self):
        """Vrátí True pokud je daná matice hermitovská v opačném případě vrátí False, pokud není čtvercová vyhodí chybu"""
        if self.square() == False:
            raise Exception("Not valid matrix to decide from")
        i = 0
        j = 0
        while i < self.m:
            j = 0
            while j < self.n:
                if self.matrix[i][j].imaginary != -self.matrix[j][i].imaginary:
                    return False
                j = j + 1
            i = i + 1
        return True
    def isLine(self):
        """Vrátí True pokud je daná řádková jednotková v opačném případě vrátí False"""
        if self.m == 1:
            return True
        return False
    def isColumn(self):
        """Vrátí True pokud je daná matice sloupcová v opačném případě vrátí False"""
        if self.n == 1:
            return True
        return False
    def isUpTriangular(self, ctvercova = True):
        """Vrátí True pokud je daná matice horní trojúhelníková v opačném případě vrátí False, pokud není čtvercová vyhodí chybu"""
        if self.square() == False and ctvercova == True:
            raise Exception("Not valid matrix to decide from")
        i = 0
        j = 0
        while i < self.m:
            j = 0
            while j < self.n:
                if i > j:
                    if self.matrix[i][j].real != 0:
                        return False
                    if self.matrix[i][j].imaginary != 0:
                        return False
                j = j + 1
            i = i + 1
        return True
    def isDownTriangular(self, ctvercova = True):
        """Vrátí True pokud je daná matice dolní trojúhelniíková v opačném případě vrátí False, pokud není čtvercová vyhodí chybu"""
        if self.transpose().isUpTriangular(ctvercova):
            return True
        return False
    def sumLine(self, a, b):
        """Vrátí matici, kde k původní matici přičetl řádek s indexem a k řádku s indexem b"""
        c = copy.deepcopy(self)
        i = 0
        while i < c.n:
            c.matrix[b][i].real = c.matrix[b][i].real + c.matrix[a][i].real
            c.matrix[b][i].imaginary = c.matrix[b][i].imaginary + c.matrix[a][i].imaginary
            i = i + 1
        return c
    def noneNull(self, a):
        """Vrátí první nenulovou hodnotu na daném řádku"""
        i = 0
        while i < self.n:
            if self.matrix[a][i].real != 0 or self.matrix[a][i].imaginary != 0:
                return self.matrix[a][i]
            i = i + 1
        return None
    def firstNoneNull(self, a):
        """Vrátí index první nenulové hodnoty na daném řádku"""
        i = 0
        while i < self.n:
            if self.matrix[a][i].real != 0 or self.matrix[a][i].imaginary != 0:
                return i
            i = i + 1
        return None
    def swap(self, a, b):
        """Vrátí matici, kde prohodí řádek a s řádkem b"""
        c = copy.deepcopy(self)
        c.matrix[a], c.matrix[b] = c.matrix[b], c.matrix[a]
        return c
    def lineSkalar(self, skalar, b):
        """Vrátí matici, kde řádek b vynásobí skalárem"""
        a = copy.deepcopy(self)
        if type(skalar) != Complex:
            skalar = Complex(skalar, 0)
        c = []
        d = []
        i = 0
        j = 0
        while i < a.m:
            j = 0
            while j < a.n:
                if i == b:
                    c.append(Complex.multiply(skalar, a.matrix[i][j]))
                else:
                    c.append(Complex.multiply(Complex(1, 0), a.matrix[i][j]))
                j = j + 1
            d.append(c)
            c = []
            i = i + 1
        newMatrix = Matrix(d)
        return newMatrix
    def deleteNullRow(self):
        """Vrátí matici z, které dal pryč nenulové řádky"""
        a = copy.deepcopy(self)
        i = 0
        while i < a.m:
            if a.noneNull(i) == None:
                a.matrix.pop(i)
                a.m = a.m - 1
            else:
                i = i + 1
        return a
    def deleteNullColumnFromLeft(self):
        """Vrátí matici z, které dal pryč nenulové sloupce zleva dokud nenarazí na nenulový sloupec"""
        a = copy.deepcopy(self)
        i = 0
        helper = True
        while i < a.m:
            if a.firstNoneNull(i) == 0:
                helper = False
            i = i + 1
        i = 0
        if helper == True:
            while i < a.m:
                a.matrix[i].pop(0)
                i = i + 1
            a.n = a.n - 1
            a.deleteNullColumnFromLeft()
        return a
    def sort(self):
        """Vrátí danou matici seřazenou, tak že je matice seřazena do rádobi trojúhelníkové"""
        a = self
        j = 0
        while j < a.m:
            i = j + 1
            highest = a.firstNoneNull(j)
            index = j
            while i < a.m:
                if highest > a.firstNoneNull(i):
                    index = i
                    highest = a.firstNoneNull(i)
                i = i + 1
            a.swap(j, index)
            j = j + 1
        return a
    def canBeInt(self):
        """Vrátí danou matici, kde všechna čísla co jsou float ale nemají desetiný rozvoj dá na inty"""
        a = self
        i = 0
        j = 0
        while i < a.m:
            j = 0
            while j < a.n:
                if type(a.matrix[i][j].real) != int:
                    if str(a.matrix[i][j].real).split(".")[1] == "0":
                        a.matrix[i][j].real = int(a.matrix[i][j].real)
                if type(a.matrix[i][j].imaginary) != int:
                    if str(a.matrix[i][j].imaginary).split(".")[1] == "0":
                        a.matrix[i][j].imaginary = int(a.matrix[i][j].imaginary)
                j = j + 1
            i = i + 1
        return a
    def toUpTriangular(self, smartRound = True):
        """Převede danou matici do trojúhelníkového tvaru. 
        Nepovinný parametr smartRound, pokud změníme na False nebude danou matici zaokrouhlovat (ovšem nemusí pak správně fungovat,
        pokud daný parametr změníme na nějaké malé číslo bude zaokrouhlovat dle něj)"""
        i = 0
        j = 0
        while i < self.m:
            j = 0
            while j < self.n:
                if self.matrix[i][j].imaginary != 0:
                    raise Exception("Cannot use complex numbers in this method")
                j = j + 1
            i = i + 1
        a = copy.deepcopy(self)
        a = a.deleteNullRow()
        a = a.deleteNullColumnFromLeft()
        a = a.sort()
        j = 0
        while j < a.m:
            i = j + 1
            while i < a.m:
                if a.firstNoneNull(i) == j:
                    x = Complex.division(a.noneNull(i), a.noneNull(j))
                    a = a.lineSkalar(Complex.multiply(Complex(-1, 0), x), j)
                    a = a.sumLine(j, i)
                    if smartRound != False:
                        a = a.roundNull(0.0000000000001)
                i = i + 1
            j = j + 1
        if smartRound != False:
            if smartRound != True:
                a = a.roundNull(smartRound)
                a = a.round(smartRound)
            if smartRound == True:
                a = a.roundNull(0.001)
                a = a.round(0.001)
        a = a.canBeInt()
        a = a.deleteNullRow()
        a = a.deleteNullColumnFromLeft()
        return a
    def rank(self):
        """Vrátí hodnost dané matice"""
        a = copy.deepcopy(self)
        return a.toUpTriangular().m
    def notGauss(self, smartRound = True):
        """Převede danou matici do tvaru z, kterého lze vyčíst řešení linearních rovnic, pokud byla mtice zadána tak, aby měla řešení. 
        Nepovinný parametr smartRound, pokud změníme na False nebude danou matici zaokrouhlovat (ovšem nemusí pak správně fungovat,
        pokud daný parametr změníme na nějaké malé číslo bude zaokrouhlovat dle něj)"""
        a = copy.deepcopy(self)
        if smartRound == True:
            a = a.toUpTriangular(0.000000000001)
        elif smartRound == False:
            a = a.toUpTriangular(False)
        else:
            a = a.toUpTriangular(smartRound)
        if a.m != a.n - 1:
            raise Exception("Cannot do this method with any matrix that is not aX(a+1)")
        if a.isUpTriangular(False) != True:
            raise Exception("There was probably some problem with racional numbers, please try to compute with smartRound = True")
        index = 1
        i = 1
        while index != 0:
            index = a.firstNoneNull(a.m - i)
            x = a.noneNull(a.m - i).real
            y = 1/x
            a = a.lineSkalar(y, a.m - i)
            result = a.matrix[a.m - i][-1].real
            j = 0
            while j < a.m - i:
                a.matrix[j][-1].real = a.matrix[j][-1].real - (a.matrix[j][index].real*result)
                a.matrix[j][index].real = 0
                j = j + 1
            i = i + 1
        if smartRound != False:
            if smartRound != True:
                a = a.roundNull(smartRound)
                a = a.round(smartRound)
            if smartRound == True:
                a = a.roundNull(0.001)
                a = a.round(0.001)
        a = a.canBeInt()
        return a




x = Matrix([[0, 700000, 2, 1, 8], [0, 1, 4, 2, 1], [0, 2, 1, 1, 0], [0, 0, 0, 0, 0]])
Matrix.print(x.notGauss(False))
print("-------------------------------------------------------------------------------------------------")
Matrix.print(x.notGauss())
print("Krásný příklad, jak chytré zaokrouhlení nepoškodí výsledek")
y = Matrix([[0, 7, 2, 1, 8], [0, 1, 4, 2, 1], [0, 2, 1, 1, 0], [0, 0, 0, 0, 0]])
Matrix.print(y.notGauss())